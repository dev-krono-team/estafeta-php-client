# estafeta-php-client

This library is a php soap client for estafeta web services.
Actually support createLabel and ExecuteQuery methods.

##How to use:
Add this repo in your composer.json

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/dev-krono-team/estafeta-php-client.git"
    }
  ],
  "require": {
    "dev-krono-team/estafeta-php-client": "dev-master"
  }
}
```

##Examples.

```php
require_once __DIR__ . '/../vendor/autoload.php';

use Kronoapp\Estafeta\EstafetaOperations as Client;

$client = new Client(Client::CREATE_LABEL_SB_WSDL);

$createLabel = new \Kronoapp\Estafeta\Classmap\CreateLabel();
$createLabel->EstafetaLabelRequest = new \Kronoapp\Estafeta\Classmap\EstafetaLabelRequest();
$createLabel->EstafetaLabelRequest->customerNumber = '0000000';
$createLabel->EstafetaLabelRequest->login = 'prueba1';
$createLabel->EstafetaLabelRequest->paperType = 1;
$createLabel->EstafetaLabelRequest->password = 'lAbeL_K_11';
$createLabel->EstafetaLabelRequest->quadrant = 0;
$createLabel->EstafetaLabelRequest->suscriberId = 28;
$createLabel->EstafetaLabelRequest->valid = true;
$createLabel->EstafetaLabelRequest->labelDescriptionListCount = 1;
# labelDescriptionList
$createLabel->EstafetaLabelRequest->labelDescriptionList = new \Kronoapp\Estafeta\Classmap\LabelDescriptionList();
$createLabel->EstafetaLabelRequest->labelDescriptionList->aditionalInfo = 'OPERACION5';
$createLabel->EstafetaLabelRequest->labelDescriptionList->content = 'JOYAS';
$createLabel->EstafetaLabelRequest->labelDescriptionList->contentDescription = 'ORO';
$createLabel->EstafetaLabelRequest->labelDescriptionList->costCenter = '12345';
$createLabel->EstafetaLabelRequest->labelDescriptionList->deliveryToEstafetaOffice = false;
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationCountryId = 'MX';
$createLabel->EstafetaLabelRequest->labelDescriptionList->numberOfLabels = 1;
$createLabel->EstafetaLabelRequest->labelDescriptionList->officeNum = '130';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originZipCodeForRouting = '02300';
$createLabel->EstafetaLabelRequest->labelDescriptionList->parcelTypeId = 1;
$createLabel->EstafetaLabelRequest->labelDescriptionList->reference = 'FRENTE AL SANBORNS';
$createLabel->EstafetaLabelRequest->labelDescriptionList->returnDocument = false;
$createLabel->EstafetaLabelRequest->labelDescriptionList->serviceTypeId = 70;
$createLabel->EstafetaLabelRequest->labelDescriptionList->valid = true;
$createLabel->EstafetaLabelRequest->labelDescriptionList->weight = 5;
# Destination Info
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo = new \Kronoapp\Estafeta\Classmap\DestinationInfo();
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->address1 = 'MAIZALES';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->address2 = '35';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->cellPhone = '4444444';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->city = 'COYOACAN';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->contactName = 'JAVIER SANCHEZ';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->corporateName = 'CHICOLOAPAN SA DE CV';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->customerNumber = '0000000';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->neighborhood = 'CENTRO';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->phoneNumber = '777777';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->state = 'ESTADO  DE MEXICO';
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->valid = true;
$createLabel->EstafetaLabelRequest->labelDescriptionList->destinationInfo->zipCode = '02130';
# Original Info
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo = new \Kronoapp\Estafeta\Classmap\OriginInfo();
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->address1 = 'CALLE 5';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->address2 = '29';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->cellPhone = '888888';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->city = 'TLALPAN';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->contactName = 'JANET OIDOR';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->corporateName = 'ALTAS SA DE CV';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->customerNumber = '0000000';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->neighborhood = 'CENTRO';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->phoneNumber = '9999999';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->state = 'DF';
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->valid = true;
$createLabel->EstafetaLabelRequest->labelDescriptionList->originInfo->zipCode = '02300';
# DRAlternativeInfo
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo = new \Kronoapp\Estafeta\Classmap\DRAlternativeInfo();
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->address1 = 'CERRADA DE CEYLAN';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->address2 = '539';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->cellPhone = '55555555';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->city = 'AZCAPOTZALCO';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->contactName = 'CARLOS MATEOS';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->corporateName = 'INTERNET SA  DE CV';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->customerNumber = '0000000';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->neighborhood = 'INDUSTRIA';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->phoneNumber = '6666666666';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->state = 'DF';
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->valid = true;
$createLabel->EstafetaLabelRequest->labelDescriptionList->DRAlternativeInfo->zipCode = '02300';
$response = $client->createLabel($createLabel);
```

```php
require_once __DIR__ . '/../vendor/autoload.php';

use Kronoapp\Estafeta\EstafetaOperations as Client;

$client = new Client(Client::CHECK_SHIPPING_WSDL);

$waybillList = new \Kronoapp\Estafeta\Classmap\WaybillList();
$waybillList->waybillType = $waybillList::LABEL;
$waybillList->waybills = ['0009999999104930237765', '0011011300120581214574'];

$searchType = new \Kronoapp\Estafeta\Classmap\SearchType();
$searchType->waybillRange = new \Kronoapp\Estafeta\Classmap\WaybillRange();
$searchType->waybillList = $waybillList;
$searchType->type = $searchType::ST_LIST;

$historyConfiguration = new \Kronoapp\Estafeta\Classmap\HistoryConfiguration();
$historyConfiguration->includeHistory = 1;
$historyConfiguration->historyType = $historyConfiguration::ALL;

$filter = new \Kronoapp\Estafeta\Classmap\Filter();
$filter->filterInformation = 0;
$filter->filterType = $filter::DELIVERED;

$searchConfiguration = new \Kronoapp\Estafeta\Classmap\SearchConfiguration();
$searchConfiguration->includeDimensions = 1;
$searchConfiguration->includeWaybillReplaceData = 0;
$searchConfiguration->includeReturnDocumentData = 0;
$searchConfiguration->includeMultipleServiceData = 0;
$searchConfiguration->includeInternationalData = 0;
$searchConfiguration->includeSignature = 0;
$searchConfiguration->includeCustomerInfo = 1;
$searchConfiguration->historyConfiguration = $historyConfiguration;
$searchConfiguration->filterType= $filter;

$executeQuery = new \Kronoapp\Estafeta\Classmap\ExecuteQuery();
$executeQuery->searchType = $searchType;
$executeQuery->searchConfiguration = $searchConfiguration;
$executeQuery->login = 'Usuario1';
$executeQuery->password = '1GCvGIu$';
$executeQuery->suscriberId = 25;

$client->executeQuery($executeQuery);
```

