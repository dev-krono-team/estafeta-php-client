<?php


namespace Kronoapp\Estafeta\Classmap;


class DeliveryData
{

    /**
     * @var string
     */
    public $destinationAcronym;

    /**
     * @var string
     */
    public $destinationName;

    /**
     * @var string
     */
    public $deliveryDateTime;

    /**
     * @var string
     */
    public $zipCode;

    /**
     * @var string
     */
    public $receiverName;
}