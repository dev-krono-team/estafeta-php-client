<?php


namespace Kronoapp\Estafeta\Classmap;


class PickupData
{

    /**
     * @var string
     */
    public $originAcronym;

    /**
     * @var string
     */
    public $originName;

    /**
     * @var string
     */
    public $pickupDateTime;
}