<?php


namespace Kronoapp\Estafeta\Classmap;


class WaybillReplaceData
{

    /**
     * @var string
     */
    public $originalWaybill;

    /**
     * @var string
     */
    public $replaceWaybill;
}