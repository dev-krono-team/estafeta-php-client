<?php


namespace Kronoapp\Estafeta\Classmap;


class MultipleServiceData
{

    /**
     * @var string
     */
    public $precedingWaybills;

    /**
     * @var string
     */
    public $followingWaybills;


    /**
     * @var string[]
     */
    public $waybillList;


}