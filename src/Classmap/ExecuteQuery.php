<?php


namespace Kronoapp\Estafeta\Classmap;


class ExecuteQuery
{
    /**
     * @var int
     */
    public $suscriberId;

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var SearchType
     */
    public $searchType;

    /**
     * @var SearchConfiguration
     */
    public $searchConfiguration;
}