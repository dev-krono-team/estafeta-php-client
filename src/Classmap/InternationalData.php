<?php


namespace Kronoapp\Estafeta\Classmap;


class InternationalData
{

    /**
     * @var string
     */
    public $internationalWaybill;

    /**
     * @var string
     */
    public $originCountryCode;

    /**
     * @var string
     */
    public $originCountrySPA;

    /**
     * @var string
     */
    public $originCountryENG;

}