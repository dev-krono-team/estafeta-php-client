<?php


namespace Kronoapp\Estafeta\Classmap;


class WaybillRange
{
    /**
     * @var string
     */
    public $initialWaybill = '';

    /**
     * @var string
     */
    public $finalWaybill = '';
}