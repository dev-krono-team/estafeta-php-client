<?php


namespace Kronoapp\Estafeta\Classmap;


class EstafetaLabelRequest extends LabelRequest
{
    /**
     * @var LabelDescriptionList
     */
    public $labelDescriptionList;

    /**
     * @var int
     */
    public $labelDescriptionListCount;
}