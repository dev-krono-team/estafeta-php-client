<?php


namespace Kronoapp\Estafeta\Classmap;


class History
{

    /**
     * @var string
     */
    public $eventDateTime;

    /**
     * @var string
     */
    public $eventId;

    /**
     * @var string
     */
    public $eventDescriptionSPA;

    /**
     * @var string
     */
    public $eventDescriptionENG;

    /**
     * @var string
     */
    public $eventPlaceAcronym;

    /**
     * @var string
     */
    public $eventPlaceName;

    /**
     * @var string
     */
    public $exceptionCode;

    /**
     * @var string
     */
    public $exceptionCodeDescriptionSPA;

    /**
     * @var string
     */
    public $exceptionCodeDescriptionENG;

    /**
     * @var string
     */
    public $exceptionCodeDetails;


}