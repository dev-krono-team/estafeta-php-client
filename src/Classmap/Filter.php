<?php


namespace Kronoapp\Estafeta\Classmap;


class Filter
{
    const DELIVERED = 'DELIVERED';
    const ON_TRANSIT = 'ON_TRANSIT';
    const RETURNED = 'RETURNED';

    /**
     * @var bool
     */
    public $filterInformation;

    /**
     * @var string
     */
    public $filterType;

}