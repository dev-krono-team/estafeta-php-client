<?php


namespace Kronoapp\Estafeta\Classmap;


class SearchType
{

    const ST_RANGE = 'R';
    const ST_LIST = 'L';

    /**
     * @var WaybillRange
     */
    public $waybillRange;


    /**
     * @var WaybillList
     */
    public $waybillList;

    /**
     * @var string
     */
    public $type;
}