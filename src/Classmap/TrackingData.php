<?php


namespace Kronoapp\Estafeta\Classmap;


class TrackingData
{

    /**
     * @var string
     */
    public $waybill;

    /**
     * @var string
     */
    public $shortWaybillId;

    /**
     * @var string
     */
    public $serviceId;

    /**
     * @var string
     */
    public $serviceDescriptionSPA;

    /**
     * @var string
     */
    public $serviceDescriptionENG;

    /**
     * @var string
     */
    public $customerNumber;

    /**
     * @var string
     */
    public $packageType;

    /**
     * @var string
     */
    public $additionalInformation;

    /**
     * @var string
     */
    public $statusSPA;

    /**
     * @var string
     */
    public $statusENG;

    /**
     * @var PickupData
     */
    public $pickupData;

    /**
     * @var DeliveryData
     */
    public $deliveryData;

    /**
     * @var Dimensions
     */
    public $dimensions;

    /**
     * @var WaybillReplaceData
     */
    public $waybillReplaceData;

    /**
     * @var ReturnDocumentData
     */
    public $returnDocumentData;

    /**
     * @var MultipleServiceData
     */
    public $multipleServiceData;

    /**
     * @var InternationalData
     */
    public $internationalData;

    /**
     * @var CustomerInfo
     */
    public $CustomerInfo;

    /**
     * @var History[]
     */
    public $history;

    /**
     * @var string
     */
    public $signature;


}