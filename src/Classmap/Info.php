<?php


namespace Kronoapp\Estafeta\Classmap;


class Info
{

    /**
     * @var string
     */
    public $address1;

    /**
     * @var string
     */
    public $address2;

    /**
     * @var string
     */
    public $cellPhone;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $contactName;

    /**
     * @var string
     */
    public $corporateName;

    /**
     * @var string
     */
    public $customerNumber;

    /**
     * @var string
     */
    public $neighborhood;

    /**
     * @var string
     */
    public $phoneNumber;

    /**
     * @var string
     */
    public $state;

    /**
     * @var bool
     */
    public $valid;

    /**
     * @var string
     */
    public $zipCode;
}