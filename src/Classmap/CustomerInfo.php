<?php


namespace Kronoapp\Estafeta\Classmap;


class CustomerInfo
{

    /**
     * @var string
     */
    public $reference;

    /**
     * @var string
     */
    public $costsCentre;
}