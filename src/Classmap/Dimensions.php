<?php


namespace Kronoapp\Estafeta\Classmap;


class Dimensions
{

    /**
     * @var string
     */
    public $weight;

    /**
     * @var string
     */
    public $volumetricWeight;

    /**
     * @var string
     */
    public $width;

    /**
     * @var string
     */
    public $length;

    /**
     * @var string
     */
    public $height;
}