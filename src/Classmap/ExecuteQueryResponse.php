<?php


namespace Kronoapp\Estafeta\Classmap;


class ExecuteQueryResponse
{

    /**
     * @var ExecuteQueryResult
     */
    public $ExecuteQueryResult;
}