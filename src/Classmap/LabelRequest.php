<?php


namespace Kronoapp\Estafeta\Classmap;


class LabelRequest
{

    /**
     * @var int
     */
    public $quadrant;

    /**
     * @var bool
     */
    public $valid;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $customerNumber;

    /**
     * @var string
     */
    public $suscriberId;

    /**
     * @var int
     */
    public $paperType;
}