<?php


namespace Kronoapp\Estafeta\Classmap;


class SearchConfiguration
{

    /**
     * @var bool
     */
    public $includeDimensions;

    /**
     * @var bool
     */
    public $includeWaybillReplaceData;

    /**
     * @var bool
     */
    public $includeReturnDocumentData;

    /**
     * @var bool
     */
    public $includeMultipleServiceData;

    /**
     * @var bool
     */
    public $includeInternationalData;

    /**
     * @var bool
     */
    public $includeSignature;

    /**
     * @var bool
     */
    public $includeCustomerInfo;

    /**
     * @var HistoryConfiguration
     */
    public $historyConfiguration;

    /**
     * @var Filter
     */
    public $filterType;
}