<?php


namespace Kronoapp\Estafeta\Classmap;


class ExecuteQueryResult
{

    /**
     * @var string
     */
    public $errorCode;

    /**
     * @var string
     */
    public $errorCodeDescriptionSPA;

    /**
     * @var string
     */
    public $errorCodeDescriptionENG;

    /**
     * @var TrackingData
     */
    public $trackingData;
}