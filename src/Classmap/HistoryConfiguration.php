<?php


namespace Kronoapp\Estafeta\Classmap;


class HistoryConfiguration
{

    const ALL = 'ALL';
    const ONLY_EXCEPTIONS = 'ONLY_EXCEPTIONS';
    const LAST_EVENT = 'LAST_EVENT';

    /**
     * @var bool
     */
    public $includeHistory;

    /**
     * @var string
     */
    public $historyType;
}