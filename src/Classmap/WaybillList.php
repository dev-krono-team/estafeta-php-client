<?php


namespace Kronoapp\Estafeta\Classmap;


class WaybillList
{

    const TRACKED_CODE = 'R';
    const LABEL = 'G';

    /**
     * @var string
     */
    public $waybillType;

    /**
     * @var array
     */
    public $waybills;
}