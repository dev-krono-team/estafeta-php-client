<?php


namespace Kronoapp\Estafeta\Classmap;


class LabelDescriptionList
{

    /**
     * @var string
     */
    public $aditionalInfo;

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $contentDescription;

    /**
     * @var string
     */
    public $costCenter;

    /**
     * @var bool
     */
    public $deliveryToEstafetaOffice;

    /**
     * @var string
     */
    public $destinationCountryId;

    /**
     * @var int
     */
    public $numberOfLabels;

    /**
     * @var string
     */
    public $officeNum;

    /**
     * @var string
     */
    public $originZipCodeForRouting;

    /**
     * @var int
     */
    public $parcelTypeId;

    /**
     * @var string
     */
    public $reference;

    /**
     * @var bool
     */
    public $returnDocument;

    /**
     * @var int
     */
    public $serviceTypeId;

    /**
     * @var bool
     */
    public $valid;

    /**
     * @var int
     */
    public $weight;

    /**
     * @var DestinationInfo
     */
    public $destinationInfo;

    /**
     * @var OriginInfo
     */
    public $originInfo;

    /**
     * @var DRAlternativeInfo
     */
    public $DRAlternativeInfo;
}