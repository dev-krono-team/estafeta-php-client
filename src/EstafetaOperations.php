<?php


namespace Kronoapp\Estafeta;


use Kronoapp\Estafeta\Classmap\{
    CreateLabel,
    CustomerInfo,
    DeliveryData,
    DestinationInfo,
    Dimensions,
    DRAlternativeInfo,
    EstafetaLabelRequest,
    ExecuteQuery,
    ExecuteQueryResponse,
    ExecuteQueryResult,
    Filter,
    History,
    HistoryConfiguration,
    InternationalData,
    LabelDescriptionList,
    MultipleServiceData,
    OriginInfo,
    PickupData,
    ReturnDocumentData,
    SearchConfiguration,
    SearchType,
    WaybillList,
    WaybillRange,
    WaybillReplaceData
};
use SoapFault;

class EstafetaOperations extends EstafetaClient
{

    /**
     * @var string[]
     */
    protected $classmap = [
        'CreateLabel' => CreateLabel::class,
        'CustomerInfo' => CustomerInfo::class,
        'DeliveryData' => DeliveryData::class,
        'DestinationInfo' => DestinationInfo::class,
        'DRAlternativeInfo' => DRAlternativeInfo::class,
        'Dimensions' => Dimensions::class,
        'EstafetaLabelRequest' => EstafetaLabelRequest::class,
        'ExecuteQuery' => ExecuteQuery::class,
        'ExecuteQueryResponse' => ExecuteQueryResponse::class,
        'ExecuteQueryResult' => ExecuteQueryResult::class,
        'Filter' => Filter::class,
        'HistoryConfiguration' => HistoryConfiguration::class,
        'History' => History::class,
        'InternationalData' => InternationalData::class,
        'LabelDescriptionList' => LabelDescriptionList::class,
        'MultipleServiceData' => MultipleServiceData::class,
        'OriginInfo' => OriginInfo::class,
        'PickupData' => PickupData::class,
        'ReturnDocumentData' => ReturnDocumentData::class,
        'SearchConfiguration' => SearchConfiguration::class,
        'SearchType' => SearchType::class,
        'WaybillList' => WaybillList::class,
        'WaybillRange' => WaybillRange::class,
        'WaybillReplaceData' => WaybillReplaceData::class,
    ];

    /**
     * EstafetaOperations constructor.
     * @param string|null $wsdl
     * @throws SoapFault
     */
    public function __construct(string $wsdl = null)
    {
        parent::__construct($wsdl);
    }

    /**
     * @param ExecuteQuery $executeQuery
     * @return ExecuteQueryResponse
     */
    public function executeQuery(ExecuteQuery $executeQuery): ExecuteQueryResponse
    {
        return $this->makeSoapCall(ucfirst(__FUNCTION__), $executeQuery);
    }

    /**
     * @param CreateLabel $createLabel
     * @return mixed
     */
    public function createLabel(CreateLabel $createLabel)
    {
        return $this->makeSoapCall(__FUNCTION__, (array) $createLabel, false);
    }

}