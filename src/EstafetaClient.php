<?php

declare(strict_types=1);


namespace Kronoapp\Estafeta;


use SoapClient;
use SoapFault;

class EstafetaClient
{

    const CHECK_SHIPPING_WSDL = 'https://trackingqa.estafeta.com/Service.asmx?wsdl';
    const CREATE_LABEL_SB_WSDL = 'https://labelqa.estafeta.com/EstafetaLabel20/services/EstafetaLabelWS?wsdl';
    const CREATE_LABEL_WSDL = 'https://label.estafeta.com/EstafetaLabel20/services/EstafetaLabelWS?wsdl';

    /**
     * @var array
     */
    protected $classmap;

    /**
     * @var SoapClient
     */
    protected $client;

    /**
     * @var array
     */
    private $soapHeaders;

    /**
     * EstafetaClient constructor.
     * @param string $wsdl
     * @throws SoapFault
     */
    public function __construct(string $wsdl)
    {
        $options['classmap'] = $this->classmap;
        $options['trace'] = true;
        $options['connection_timeout'] = 10000;
        $options['cache_wsdl'] = WSDL_CACHE_BOTH;
        $options['compression'] = SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE;
        $options['keep_alive'] = true;
        $options['features'] = SOAP_SINGLE_ELEMENT_ARRAYS;
        $options['user_agent'] = sprintf('PHP-SOAP/%s + Estafeta PHP Toolkit V0.1', phpversion());
        $this->client = new SoapClient($wsdl, $options);
    }

    /**
     * @param $operation
     * @param $parameter
     * @param bool $wrapped
     * @return mixed
     */
    protected function makeSoapCall($operation, $parameter, $wrapped = true)
    {
        $parameter = $wrapped ? array($parameter) : $parameter;
        return $this->client->__soapCall($operation, $parameter, NULL, $this->soapHeaders);
    }

}